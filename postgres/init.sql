CREATE ROLE auditor;
SET pgaudit.role TO 'auditor';
ALTER SYSTEM SET pgaudit.role = 'auditor, postgres';
alter system set pgaudit.log to 'WRITE, ROLE, DDL, MISC_SET';
alter system set pgaudit.log_client to 'on';
alter system set pgaudit.log_statement_once to 'on';
SELECT pg_reload_conf();